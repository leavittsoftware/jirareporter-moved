require("./node_modules/test_server/project/BASE.js");
var connectToMongo = require("./connectToMongo.js");
var totalHours = require("./totalHours.js");

BASE.require.loader.setRoot("./");
var Future = BASE.async.Future;

var rates = {
  "friendly": {
    "support": 60,
    "development": 90
  },
  "external": {
    "support": 90,
    "development": 120
  },
  "unpaid": {
    "support": 0,
    "development": 0
  }
};

var projectCategories = {
  //"Archive": 10001,
  "In Development": 10000,
  "In Production": 10200,
  "Internal": 10100,
  "OSS": 10101,
  "Support Only": 10201,
  "In Development External": 0,
  "In Production External": 0
};

var issueTypes = [
  "Bug",
  "New Feature",
  "Task",
  "Improvement",
  "Sub-task",
  "Epic",
  "Story",
  "Quick Project",
  "User Feedback"
];

var issueTypeMappings = {
  "In Development": {
    "schedule": "friendly",
    "issueTypes": {
      "Bug":           "development",
      "New Feature":   "development",
      "Task":          "development",
      "Improvement":   "development",
      "Sub-task":      "development",
      "Epic":          "development",
      "Story":         "development",
      "Quick Project": "development",
      "User Feedback": "development"
    }
  },
  "In Production": {
    "schedule": "friendly",
    "issueTypes": {
      "Bug":           "support",
      "New Feature":   "development",
      "Task":          "development",
      "Improvement":   "development",
      "Sub-task":      "development",
      "Epic":          "development",
      "Story":         "development",
      "Quick Project": "development",
      "User Feedback": "development"
    }
  },
  "Internal": {
    "schedule": "unpaid",
    "issueTypes": {
      "Bug":           "development",
      "New Feature":   "development",
      "Task":          "development",
      "Improvement":   "development",
      "Sub-task":      "development",
      "Epic":          "development",
      "Story":         "development",
      "Quick Project": "development",
      "User Feedback": "development"
    }
  },
  "OSS": {
    "schedule": "unpaid",
    "issueTypes": {
      "Bug":           "development",
      "New Feature":   "development",
      "Task":          "development",
      "Improvement":   "development",
      "Sub-task":      "development",
      "Epic":          "development",
      "Story":         "development",
      "Quick Project": "development",
      "User Feedback": "development"
    }
  },
  "Support Only": {
    "schedule": "friendly",
    "issueTypes": {
      "Bug":           "support",
      "New Feature":   "support",
      "Task":          "support",
      "Improvement":   "support",
      "Sub-task":      "support",
      "Epic":          "support",
      "Story":         "support",
      "Quick Project": "support",
      "User Feedback": "support"
    }
  },
  "Default": {
    "schedule": "unpaid",
    "issueTypes": {
      "Bug":           "development",
      "New Feature":   "development",
      "Task":          "development",
      "Improvement":   "development",
      "Sub-task":      "development",
      "Epic":          "development",
      "Story":         "development",
      "Quick Project": "development",
      "User Feedback": "development"
    }
  }
};

var billingReport = function(query){
  return new Future(function(setValue, setError){
    query = query || {};
    connectToMongo().then(function(db){
      var collection = db.collection("issues");

      var projects = {};
      var emptyProject = function(category){
        var self = this;
        self.category = category;
        self.support = {"hours":0, "rate":0, "total":0 };
        self.development = {"hours":0, "rate":0, "total":0 };
      };

      collection.find(query).toArray(function(error, results){
        results.forEach(function(result){
          var projectName = result.fields.project.key;
          var projectCategory = result.fields.project.projectCategory.name;
          if (typeof projectCategory === "undefined") {
            console.warn("Category not set for project: " + projectName);
            projectCategory = "Default";
          }
          var project = projects[projectName] = projects[projectName] || new emptyProject(projectCategory);

          var issueType = result.fields.issuetype.name;
          var mapping = issueTypeMappings[projectCategory];
          if (typeof mapping === "undefined") {
            console.warn("Couldn't find category: " + projectCategory);
          } else {
            var rateType = mapping.issueTypes[issueType];
            if (typeof result.fields.worklog !== "undefined" && result.fields.worklog.total > 0) {
              result.fields.worklog.worklogs.forEach(function(worklog){
                if (typeof project[rateType] === "undefined") {
                  console.warn("Project " + projectName + " has no rate type for issue type " + issueType);
                } else {
                  project[rateType].hours += worklog.timeSpentSeconds / 60 / 60;
                }
              });
            }
          }
        });

        Object.keys(projects).forEach(function(projectName){
          var project = projects[projectName];
          var schedule = issueTypeMappings[project.category].schedule;
          project.support.rate = rates[schedule].support;
          project.development.rate = rates[schedule].development;
          project.support.total = project.support.hours * project.support.rate;
          project.support.total = (Math.round(project.support.total * 10)/10);
          project.development.total = project.development.hours * project.development.rate;
          project.development.total = (Math.round(project.development.total * 10)/10);
        });

        setValue(projects);
      });
    });

  });
};

module.exports = billingReport;
