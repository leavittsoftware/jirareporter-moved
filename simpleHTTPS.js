var https = require("https");
var url = require("url");

module.exports = {
  request: function(location, options){
    return new BASE.async.Future(function(setValue, setError){
      var parsedUrl = url.parse(location);
      options.hostname = parsedUrl.host;
      options.method = options.method || "GET";
      options.path = parsedUrl.path;

      var value = "";

      var request = https.request(options, function(res){
        res.setEncoding("utf8");
        res.on("data", function(chunk){
          value += chunk;
        });
        res.on("end", function(){
          setValue(value);
        });
      });

      request.end();

    });
  }
};
