module.exports = function(issues){
  var seconds = 0;
  issues.forEach(function(issue){
    if (typeof issue.fields.worklog !== "undefined") {
      issue.fields.worklog.worklogs.forEach(function(worklog){
        seconds += worklog.timeSpentSeconds;
      });
    }
  });
  return (seconds / 60) / 60;
};
