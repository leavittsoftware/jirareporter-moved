require("./node_modules/test_server/project/BASE.js");
var totalHours = require("./totalHours.js");
var connectToMongo = require('./connectToMongo.js');

BASE.require.loader.setRoot("./");
var Future = BASE.async.Future;

var hoursHandler = function(db, query){
  return new Future(function(setValue, setError){
    var collection = db.collection("issues");
    collection.find(JSON.parse(query)).toArray(function(error, docs){
      if (error) {
        setError(error);
      } else {
        console.log("Found " + docs.length + " issues.");
        setValue(totalHours(docs));
      }
    });
  });
};


var handlers = {
  "hours":hoursHandler
};

var queryIssues = function(type, query){

  return new Future(function(setValue, setError){
    connectToMongo().then(function(db){
      handlers[type](db, query).then(function(result){
        setValue(result);
      });
    });
  });

};




 //db.issues.find({"fields.worklog.worklogs.author.key":"aaron-drabeck"})
