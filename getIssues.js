require("./node_modules/test_server/project/BASE.js");
var simpleHTTPS = require("./simpleHTTPS.js");
var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;

BASE.require.loader.setRoot(__dirname + "/node_modules/test_server/project/");
var Future = BASE.async.Future;

var mongoURL = 'mongodb://localhost:27017/jirastats';
var jiraInstanceURL = "https://leavittsoftware.atlassian.net/";
var jiraAPIPath = "rest/api/2/search";
var jira = jiraInstanceURL + jiraAPIPath;
var issuesPerQuery = 1000;
var jiraIssuesQuery = "?jql=category not in (\"Archive\")&fields=issuetype,worklog,project";
var mongoDb = null;

var jiraRequestOptions = {
  "headers": {
    "accept": "application/json",
    "authorization": ""
  }
};

var getIssueQuery = function(start, count){
  return jiraIssuesQuery + "&startAt=" + start + "&maxResults=" + count;
};

var getAuthString = function(){
  var authString = "";
  // try to read from authstring.txt
  if (fs.existsSync("authstring.txt")) {
    authString = fs.readFileSync("authstring.txt", "utf-8").trim();
  } else {
    authString = process.env.JIRA_REPORTER_AUTH;
  }

  return authString;
};

var connectToMongo = function(){
  return new Future(function(setValue, setError){
  MongoClient.connect(mongoURL, function(error, db){
    if (error) {
      setError(error);
    } else {
      setValue(db);
    }
  });
})};

var prepareIssue = function(issue){
  // convert date strings into date objects
  issue.fields.worklog.worklogs.forEach(function(worklog){
    worklog.created = new Date(Date.parse(worklog.created));
    worklog.updated = new Date(Date.parse(worklog.updated));
    worklog.started = new Date(Date.parse(worklog.started));
  });
};

var saveIssuesToDb = function(issues) {
  var collection = mongoDb.collection("issues");
  return new Future(function(setValue, setError){
    // TODO: Get the rest of the issue worklogs if there are more than 20
    issues.forEach(prepareIssue);
    collection.insert(issues, function(error, result){
      if (error) {
        console.log(error);
        setError(error);
      } else {
        setValue(result);
      }
    });
  });
};

var clearIssuesCollection = function(){
  return new Future(function(setValue, setError){
    var collection = mongoDb.collection("issues");
    collection.remove({}, function(error, result){
      if (error) {
        console.log(error);
        setError(error);
      } else {
        setValue(result);
      }
    });
  });
};

var getIssues = function(){
  return new Future(function(setValue, setError){
    BASE.require(["BASE.async.Task"], function(){
      connectToMongo().chain(function(db){
        mongoDb = db;
        return clearIssuesCollection();
      }).chain(function(auth){
        jiraRequestOptions.headers.authorization = getAuthString();
        return simpleHTTPS.request(jira + getIssueQuery(0,0), jiraRequestOptions);
      }).then(function(countResponse){
        var count = JSON.parse(countResponse).total;
        console.log("Found: " + count);
        var pen = 0;
        var getIssuesTask = new BASE.async.Task();

        while (pen < count) {
          getIssuesTask.add(simpleHTTPS.request(jira + getIssueQuery(pen, issuesPerQuery), jiraRequestOptions).chain(function(queryResult){
            var parsed = JSON.parse(queryResult);
            var issues = parsed.issues;
            return saveIssuesToDb(issues);
          }));
          pen += issuesPerQuery - 1;
        }

        getIssuesTask.start().whenAll(function(futures){
          setValue();
        });
      });
    });
  });
};

module.exports = getIssues;
