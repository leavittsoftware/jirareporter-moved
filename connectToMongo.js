require("./node_modules/test_server/project/BASE.js");
var MongoClient = require('mongodb').MongoClient;
var Future = BASE.async.Future;

var mongoURL = 'mongodb://localhost:27017/jirastats';

module.exports = function(){
  return new Future(function(setValue, setError){
  MongoClient.connect(mongoURL, function(error, db){
    if (error) {
      setError(error);
    } else {
      setValue(db);
    }
  });
})};
